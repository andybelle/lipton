<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->tinyInteger('type')->default(1)->comment('欄目類型: 1. 標準文章, 2. 外部鏈結');
            $table->tinyInteger('order')->default(5)->comment('排序');
            $table->string('external_link')->nullable()->comment('外部連結');
            $table->boolean('visible')->default(true)->comment('是否顯示');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_categories');
    }
}
