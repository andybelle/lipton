<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsCrawlerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_crawlers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('cms_category_id')->nullable();
            $table->string('domain')->nullable();
            $table->tinyInteger('image_solution')->default(0)->comment('0:不處理, 1:下載到本地端');
            $table->tinyInteger('command_status')->default(2)->comment('手動執行狀態 0:等待執行, 1:執行中, 2:執行完畢');
            $table->tinyInteger('operation')->default(0)->comment('運行開關 0:關閉, 1:開始');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_crawlers');
    }
}
