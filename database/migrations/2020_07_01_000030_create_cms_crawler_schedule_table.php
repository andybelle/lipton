<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsCrawlerScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_crawler_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('crawler_id')->unique();
            $table->tinyInteger('method')->default(1)->comment('reference laravel schedule:method');
            $table->string('method_param')->default(null)->nullable();

            $table->foreign('crawler_id')
                ->references('id')->on('cms_crawlers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_crawler_schedule');
    }
}
