<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cms_category_id')->nullable();
            $table->unsignedBigInteger('author_id');
            $table->string('title');
            $table->string('slug')->unique()->nullable();
            $table->longText('content')->nullable();
            $table->tinyInteger('order')->nullable()->default(5)->comment('排序');
            $table->string('external_link')->nullable()->comment('外部連結');
            $table->bigInteger('score')->nullable()->default(0);
            $table->tinyInteger('status')->default(0)->comment('0: 正常');
            $table->unsignedInteger('cover_id')->nullable();
            $table->timestamps();

            $table->foreign('cms_category_id')
                ->references('id')->on('cms_categories')
                ->onDelete('cascade');
            $table->foreign('author_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('cover_id')
                ->references('id')->on('cms_media');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_posts');
    }
}
