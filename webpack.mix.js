const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
// .sass('resources/assets/sass/app.scss', 'publishable/assets/css')
// .copy('resources/assets/js', 'publishable/assets/js')
.scripts('resources/assets/js/animsition/animsition.js', 'resources/assets/js/animsition/animsition.min.js')
.copyDirectory('./node_modules/admin-lte/dist', 'resources/assets/admin-lte/dist')
.copyDirectory('./node_modules/admin-lte/plugins', 'resources/assets/admin-lte/plugins')
.disableNotifications();
