<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Prefix for admin path
    |--------------------------------------------------------------------------
    |
    | Here you can specify prefix for admin path
    |
    */
    'admin_prefix' => env('ADMIN_PREFIX', 'admin168'),

    /*
    |--------------------------------------------------------------------------
    | Path to the lipton Assets
    |--------------------------------------------------------------------------
    |
    | Here you can specify the location of the lipton assets path
    |
    */
    'assets_path' => '/vendor/kevinkao/lipton/assets',

    /*
    |--------------------------------------------------------------------------
    | Default route link
    |--------------------------------------------------------------------------
    |
    |
    */
    'default_route' => 'lipton.user.index',

    /*
    |--------------------------------------------------------------------------
    | The super admin role list
    |--------------------------------------------------------------------------
    |
    | If you set, it will be hidden anywhere, except current user has super admin role
    |
    */
    'super_admin' => 'super_admin',

    /*
    |--------------------------------------------------------------------------
    | Admin group,
    |--------------------------------------------------------------------------
    |
    | Here you can setting group who can login admin panel
    |
    */
    'admin_group' => ['super_admin', 'admin', 'kefu'],

    /*
    |--------------------------------------------------------------------------
    | Sidebar list
    |--------------------------------------------------------------------------
    */
    'nav' => [
        // 一般
        'lipton::common.nav.general' => [
            \KevinKao\Lipton\NavItems\UserIndexNavItem::class,
            \KevinKao\Lipton\NavItems\RoleIndexNavItem::class,
        ],
        'lipton::common.nav.cms' => [
            \KevinKao\Lipton\NavItems\CmsCategoryNavItem::class,
            \KevinKao\Lipton\NavItems\CmsPostNavItem::class,
            \KevinKao\Lipton\NavItems\CmsCommentNavItem::class,
            \KevinKao\Lipton\NavItems\CmsCrawlerNavItem::class,
        ],
        'lipton::common.nav.system' => [
            \KevinKao\Lipton\NavItems\SettingsNavItem::class,
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | 文章圖片放置目錄
    |--------------------------------------------------------------------------
    | /storage/app/public/{media_path}
    */
    'media_path' => 'media',


    /*
    |--------------------------------------------------------------------------
    | 爬蟲設置
    |--------------------------------------------------------------------------
    */
    'crawler' => [
        'middleware' => [
            // 清單頁
            'list' => [
                // 
            ],
            // 內容頁
            'content' => [
                \KevinKao\Lipton\CMS\Crawler\Middleware\Content\CheckDuplicate::class,
                \KevinKao\Lipton\CMS\Crawler\Middleware\Content\FixImageUrl::class,
                \KevinKao\Lipton\CMS\Crawler\Middleware\Content\ImageDownload::class,
                \KevinKao\Lipton\CMS\Crawler\Middleware\Content\StoreContent::class,
            ]
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | 預設前台route name
    |--------------------------------------------------------------------------
    */
    'cms_route' => [
        'single' => 'cms.single'
    ]
];