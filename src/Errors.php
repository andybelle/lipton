<?php

namespace KevinKao\Lipton;

class Errors
{
    const UNKNOWN_ERROR = 1;
    const VALIDATION_FAILED = 2;
    const ACCESS_DENIED = 3;
}