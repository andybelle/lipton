<?php

namespace KevinKao\Lipton\NavItems;

use Gate;

/**
* 系統設置
*/
class SettingsNavItem extends NavItem
{
    public $title = 'lipton::settings.nav.index';
    public $iconClass = 'fa-cogs';
    public $hasTreeView = false;
    protected $childItemsClass = [
        // 
    ];

    public function getLink($fullUrl = true)
    {
        return route('lipton.settings.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'settings');
    }
}
