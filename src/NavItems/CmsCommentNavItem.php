<?php

namespace KevinKao\Lipton\NavItems;

use Gate;

class CmsCommentNavItem extends NavItem
{
    public $title = 'lipton::cms.nav.comment.index';
    public $iconClass = 'fa-comments';
    public $hasTreeView = false;

    public function getLink($fullUrl = true)
    {
        return route('lipton.cms.comment.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'cms_comment');
    }
}