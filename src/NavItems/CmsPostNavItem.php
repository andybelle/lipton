<?php

namespace KevinKao\Lipton\NavItems;

use Gate;

class CmsPostNavItem extends NavItem
{
    public $title = 'lipton::cms.nav.post.index';
    public $iconClass = 'fa-clone';
    public $hasTreeView = false;

    public function getLink($fullUrl = true)
    {
        return route('lipton.cms.post.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'cms_post');
    }
}