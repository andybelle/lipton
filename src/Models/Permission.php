<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;
use KevinKao\Lipton\Models\Role;

class Permission extends Model
{
    public function roles ()
    {
        return $this->belongsToMany(Role::class);
    }
}