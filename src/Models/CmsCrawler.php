<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;

class CmsCrawler extends Model
{
    protected $fillable = ['title', 'domain', 'cms_category_id', 'image_solution', 'charset'];

    public function category()
    {
        return $this->belongsTo(CmsCategory::class, 'cms_category_id');
    }

    public function schedule()
    {
        return $this->hasOne(CmsCrawlerSchedule::class, 'crawler_id');
    }

    public function listRule()
    {
        return $this->hasOne(CmsCrawlerListRule::class, 'crawler_id');
    }

    public function contentRule()
    {
        return $this->hasMany(CmsCrawlerContentRule::class, 'crawler_id');
    }

    public function scopeOperating($query)
    {
        return $query->where('operation', 1);
    }

    public function scopePending($query)
    {
        return $query->where('command_status', 0);
    }

    public function scopeExecuting($query)
    {
        return $query->where('command_status', 1);
    }

    public function scopeSuspended($query)
    {
        return $query->where('command_status', 2);
    }
}