<?php

namespace KevinKao\Lipton\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Collection;

use KevinKao\Lipton\Facades\LiptonFacade;
use KevinKao\Lipton\Lipton;
use KevinKao\Lipton\Http\Middleware\AuthMiddleware;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Schema;

use Log;
use KevinKao\Lipton\Models\CmsCrawler;
use KevinKao\Lipton\CMS\Frequency;
use KevinKao\Lipton\CMS\Crawler\Jobs\NewsListCrawler;
use KevinKao\Lipton\CMS\Crawler\Jobs\CheckCrawlerCommand;
use KevinKao\Lipton\Console\Commands\CrawlerTrigger;
use KevinKao\Lipton\Console\Commands\CrawlerList;
use KevinKao\Lipton\Console\Commands\CrawlerCategory;
use KevinKao\Lipton\Console\Commands\CategoryList;
use KevinKao\Lipton\Console\Commands\SettingsList;
use KevinKao\Lipton\Console\Commands\SettingsUpdate;

class LiptonServiceProvider extends ServiceProvider
{
    protected $frequencyMap = [
        Frequency::EVERY_MINUTE         => 'everyMinute',
        Frequency::EVERY_FIVE_MINUTE    => 'everyFiveMinutes',
        Frequency::EVERY_TEN_MINUTE     => 'everyTenMinutes',
        Frequency::EVERY_FIFTEEN_MINUTE => 'everyFifteenMinutes',
        Frequency::EVERY_THIRTY_MINUTE  => 'everyThirtyMinutes',
        Frequency::HOURLY               => 'hourly',
        Frequency::EVERY_TWO_HOURS      => 'everyTwoHours',
        Frequency::EVERY_THREE_HOURS    => 'everyThreeHours',
        Frequency::EVERY_FOUR_HOURS     => 'everyFourHours',
        Frequency::DAILY                => 'daily',
        Frequency::WEEKLY               => 'weekly',
        Frequency::MONTHLY              => 'monthly',
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        config(['auth.providers.users.model' => \KevinKao\Lipton\Models\User::class]);

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'lipton');
        $this->loadRoutesFrom(__DIR__.'/../../routes/lipton.php');
        $this->loadTranslationsFrom(realpath(__DIR__.'/../../resources/lang'), 'lipton');
        $this->loadMigrationsFrom(realpath(__DIR__.'/../../database/migrations'));

        $router->aliasMiddleware('admin.user', AuthMiddleware::class);

        $this->app->make('config')->set('logging.channels.crawler', [
            'driver' => 'daily',
            'path' => storage_path('logs/crawler.log'),
            'level' => env('CRAWLER_LOG_LEVEL', 'info'),
            'days' => 7
        ]);

        try {
            if ($this->app->runningInConsole() && Schema::hasTable('cms_crawlers')) {

                $this->commands([
                    CategoryList::class,
                    CrawlerList::class,
                    CrawlerTrigger::class,
                    CrawlerCategory::class,
                    SettingsList::class,
                    SettingsUpdate::class
                ]);

                $this->app->booted(function() {
                    $schedule = app(Schedule::class);
                    $schedule->job(new CheckCrawlerCommand(), 'crawler.news')->everyMinute();
                    $crawlers = CmsCrawler::operating()->suspended()->get();
                    foreach($crawlers as $crawler) {
                        $method = $this->frequencyMap[$crawler->schedule->method];
                        $schedule->job(new NewsListCrawler($crawler), 'crawler.news')->$method();
                    }    
                });
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('Lipton', LiptonFacade::class);

        $this->app->singleton('lipton', function () {
            return new Lipton();
        });

        $this->loadHelpers();
        $this->registerConfigs();

        if ($this->app->runningInConsole()) {
            $this->registerPublishableResources();
        }

        Collection::macro('separate', function ($size) {
            if ($size <= 0) {
                return new static;
            }
            $items = $this->items;

            $chunks = [];
            $chunks = new static;
            $chunks[] = array_slice($items, 0, $size);
            $chunks[] = array_slice($items, $size);
            return new static($chunks);
        });
    }

    public function loadHelpers ()
    {
        foreach (glob(__DIR__.'/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

    private function registerPublishableResources()
    {
        $dir = dirname(__DIR__);

        $publishable = [
            'lipton_assets' => [
                "{$dir}/../resources/assets/js" => public_path(config('lipton.assets_path').'/js'),
                "{$dir}/../resources/assets/admin-lte" => public_path(config('lipton.assets_path').'/admin-lte'),
                "{$dir}/../resources/assets/bootstrap4-toggle" => public_path(config('lipton.assets_path').'/bootstrap4-toggle'),
            ],
            'lipton_seeds' => [
                "{$dir}/../database/seeds/" => database_path('seeds')
            ],
            'lipton_config' => [
                "{$dir}/../config/lipton.php" => config_path('lipton.php'),
            ],
        ];

        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }

    public function registerConfigs()
    {
        $this->mergeConfigFrom(dirname(__DIR__).'/../config/lipton.php', 'lipton');
    }
}