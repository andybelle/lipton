<?php

namespace KevinKao\Lipton\Http\Controllers\CMS;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

use KevinKao\Lipton\Http\Controllers\Controller;
use \KevinKao\Lipton\Models\CmsCategory;
use \KevinKao\Lipton\Models\CmsPost;
use \KevinKao\Lipton\Models\CmsMedia;
use \KevinKao\Lipton\Constants;
use \KevinKao\Lipton\Errors;

use Auth;
use DB;
use Log;
use Storage;
use Route;

class PostController extends Controller
{
    public function index()
    {
        $this->authorize('browse', 'cms_post');
        $categories = CmsCategory::all();
        return view('lipton::cms.post.index', compact('categories'));
    }

    public function datatable(Request $request)
    {
        $this->authorize('browse', 'cms_post');
        $columns = ['id', 'created_at', 'title', 'slug'];

        $recordsTotal = CmsPost::count();
        $limit  = $request->input('length');
        $start  = $request->input('start');
        $order  = $columns[$request->input('order.0.column')];
        $dir    = $request->input('order.0.dir');
        $search = $request->input('search.value');

        $searchCategoryId = $request->input('search.cms_category_id');
        $searchKeyword = $request->input('search.keyword');

        $query = CmsPost::when($searchCategoryId != 'all', function($query) use ($searchCategoryId) {
            return $query->where('cms_category_id', $searchCategoryId);
        })->when($searchKeyword != '', function($query) use ($searchKeyword) {
            return $query->where('title', 'LIKE', "%{$searchKeyword}%");
        });

        $recordsFiltered = $query->count();
        $data = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        $edit = Auth::user()->can('edit', 'cms_post');
        $delete = Auth::user()->can('delete', 'cms_post');
        foreach ($data as &$post) {
            $post->category_title = isset($post->category) ? $post->category->title : '-';

            $linkBtn = Route::has(config('lipton.cms_route.single')) ?
                sprintf(
                    '<a href="%s" class="btn bg-gradient-info btn-sm mr-1" target="_blank"><i class="fa fa-link mr-0"></i></a>',
                    route(config('lipton.cms_route.single'), $post->id)
                ) : '';
            $editAction = '<a href="' . route('lipton.cms.post.edit', $post->id) . '" class="btn bg-gradient-secondary btn-sm animsition-link">' .
                '<i class="fa fa-edit mr-0" style="width: 11px"></i></a>';
            $deleteAction = '<a href="javascript:void(0)" data-id="'.$post->id.'" class="btn bg-gradient-danger btn-sm action delete ml-1">' .
                '<i class="fa fa-trash mr-0"></i></a>';
            $post->action = $linkBtn . ($edit ? $post->action . $editAction : '') . ($delete ? $deleteAction : '');
        }
        return response()->json([
            'draw' => intval($request->input('draw')),
            'recordsTotal' => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            'data' => $data,
        ]);
    }

    public function create()
    {
        $this->authorize('create', 'cms_post');
        $categories = CmsCategory::all();
        $actionUrl = route('lipton.cms.post.store');
        return view('lipton::cms.post.create_edit', compact('categories', 'actionUrl'));
    }

    public function store(Request $request)
    {
        $this->authorize('create', 'cms_post');
        $request->validate([
            'title'           => 'required',
            'cms_category_id' => 'nullable|numeric',
            'slug'            => 'nullable|regex:/^[\w+\-]+$/',
            'order'           => 'nullable|numeric',
            'external_link'   => 'nullable|url',
            'score'           => 'nullable|numeric',
            'cover'           => 'nullable|image',
        ], [
            'title.required' => __('lipton::cms.post.feedback.is_required', ['field' => __('lipton::cms.post.form.title')]),
            'slug.regex'     => __('lipton::cms.post.feedback.invalid_slug'),
            'order.numeric'  => __('lipton::cms.post.feedback.must_be_numeric', ['field' => __('lipton::cms.post.form.order')]),
            'external_link.url'  => __('lipton::cms.post.feedback.invalid_url'),
            'score.numeric'  => __('lipton::cms.post.feedback.must_be_numeric', ['field' => __('lipton::cms.post.form.score')]),
            'cover.image'    => __('lipton::cms.post.feedback.invalid_image'),
        ]);

        try {
            DB::beginTransaction();
            if ($request->hasFile('cover')) {
                $path = Storage::url($request->file('cover')->store(config('lipton.media_path'), ['disk' => 'public']));
                $media = new CmsMedia();
                $media->path = $path;
                $media->type = Constants::CMS_MEDIA_TYPE_IMAGE;
                $media->save();
                $request->merge(['cover_id' => $media->id]);
            }

            $post = CmsPost::create([
                'cms_category_id' => $request->input('cms_category_id'),
                'author_id'       => auth()->user()->id,
                'title'           => htmlspecialchars($request->input('title')),
                'slug'            => $request->input('slug'),
                'content'         => $request->input('content'),
                'order'           => empty($request->input('order')) ? 5 : $request->input('order', 5),
                'external_link'   => $request->input('external_link'),
                'score'           => empty($request->input('score')) ? 50 : $request->input('score', 50),
                'cover_id'        => $request->input('cover_id'),
            ]);

            if ($request->has('meta')) {
                $tmpData = [];
                foreach($request->input('meta') as $item) {
                    if (empty($item['key'])) {
                        continue;
                    }
                    $tmpData[] = ['cms_post_id' => $post->id, 'k' => $item['key'], 'v' => $item['value']];
                }
                $post->meta()->createMany($tmpData);
            }

            DB::commit();
            return back()->with('success', __('lipton::cms.post.feedback.create_success'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            if (isset($media)) {
                $media->deleteFile();
            }
            return back()->withErrors([$e->getMessage()]);
        }
    }

    public function editorUpload(Request $request)
    {
        try {
            if ($request->hasFile('file')) {
                $path = Storage::url($request->file('file')->store(config('lipton.media_path'), ['disk' => 'public']));
                $media = new CmsMedia();
                $media->path = $path;
                $media->type = Constants::CMS_MEDIA_TYPE_IMAGE;
                $media->save();

                return response()->json([
                    'code' => 0,
                    'message' => 'ok',
                    'src' => $path,
                    'title' => 'title'
                ]);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', 'cms_post');
        $post = CmsPost::findOrFail($id);
        $categories = CmsCategory::all();
        $actionUrl = route('lipton.cms.post.update', $post->id);
        return view('lipton::cms.post.create_edit', compact('post', 'categories', 'actionUrl'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('edit', 'cms_post');

        $request->validate([
            'title'           => 'required',
            'cms_category_id' => 'nullable|numeric',
            'slug'            => 'nullable|regex:/^[\w+\-]+$/',
            'order'           => 'nullable|numeric',
            'external_link'   => 'nullable|url',
            'score'           => 'nullable|numeric',
            'cover'           => 'nullable|image',
        ], [
            'title.required' => __('lipton::cms.post.feedback.is_required', ['field' => __('lipton::cms.post.form.title')]),
            'slug.regex'     => __('lipton::cms.post.feedback.invalid_slug'),
            'order.numeric'  => __('lipton::cms.post.feedback.must_be_numeric', ['field' => __('lipton::cms.post.form.order')]),
            'external_link.url'  => __('lipton::cms.post.feedback.invalid_url'),
            'score.numeric'  => __('lipton::cms.post.feedback.must_be_numeric', ['field' => __('lipton::cms.post.form.score')]),
            'cover.image'    => __('lipton::cms.post.feedback.invalid_image'),
        ]);

        try {
            DB::beginTransaction();
            $post = CmsPost::findOrFail($id);
            if ($request->hasFile('cover')) {
                if (isset($post->cover_id)) {
                    $oldCoverPic = $post->cover_id;
                }
                $path = Storage::url($request->file('cover')->store(config('lipton.media_path'), ['disk' => 'public']));
                $media = new CmsMedia();
                $media->path = $path;
                $media->type = Constants::CMS_MEDIA_TYPE_IMAGE;
                $media->save();
                $post->cover_id = $media->id;
            }
            $post->title = $request->input('title');
            $post->cms_category_id = $request->input('cms_category_id');
            if ($request->has('slug'))
                $post->slug = $request->input('slug');
            if ($request->has('order'))
                $post->order = $request->input('order');
            if ($request->has('external_link'))
                $post->external_link = $request->input('external_link');
            if ($request->has('score'))
                $post->score = $request->input('score');
            $post->content = $request->input('content');
            $post->save();

            if (isset($oldCoverPic)) {
                $oldMedia = CmsMedia::find($oldCoverPic);
                $oldMedia->deleteFile();
                $oldMedia->delete();
            }

            $post->meta()->delete();
            if ($request->has('meta')) {
                $tmpData = [];
                foreach($request->input('meta') as $item) {
                    if (empty($item['key'])) {
                        continue;
                    }
                    $tmpData[] = ['cms_post_id' => $post->id, 'k' => $item['key'], 'v' => $item['value']];
                }
                $post->meta()->createMany($tmpData);    
            }
            
            DB::commit();
            return back()->with('success', __('lipton::cms.post.feedback.update_success'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return back()->withErrors([$e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', 'cms_post');

        try {
            $post = CmsPost::findOrFail($id);
            $post->delete();
            return response()->json(['code' => 0, 'message' => __('lipton::cms.post.feedback.delete_success')]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);;
        }
    }
}