<?php

namespace KevinKao\Lipton\Http\Controllers\CMS;

use Illuminate\Http\Request;
use KevinKao\Lipton\Http\Controllers\Controller;
use KevinKao\Lipton\Errors;
use KevinKao\Lipton\Models\CmsCategory;
use KevinKao\Lipton\Constants;
use Log;

class CategoryController extends Controller
{
    protected $typeMap;
    protected $visibleMap;

    public function __construct()
    {
        $this->typeMap = [
            Constants::CMS_CATEGORY_TYPE_GENERAL       => __("lipton::cms.category.type.general"),
            Constants::CMS_CATEGORY_TYPE_EXTERNAL_LINK => __("lipton::cms.category.type.external_link"),
        ];
        $this->visibleMap = [
            Constants::CMS_CATEGORY_VISIBLE_YES => __("lipton::cms.category.visible.yes"),
            Constants::CMS_CATEGORY_VISIBLE_NO => __("lipton::cms.category.visible.no"),
        ];
    }

    public function index(Request $request)
    {
        $this->authorize('browse', 'cms_category');

        if ($request->ajax()) {
            return response()->json([
                'code' => 0,
                'data' => CmsCategory::all()
            ]);
        }
        return view('lipton::cms.category.index');
    }

    public function exclude(Request $request, $id)
    {
        $this->authorize('browse', 'cms_category');

        if ($request->ajax()) {
            return response()->json([
                'code' => 0,
                'data' => CmsCategory::whereNotIn('id', [$id])->get()
            ]);
        }
    }

    public function datatable(Request $request)
    {
        $this->authorize('browse', 'cms_category');

        $columns = ['order', 'title', 'slug', 'type', 'visible'];
        $isSuperAdmin = auth()->user()->isSuperAdmin();
        
        $totalRows = CmsCategory::count();
        $totalFiltered = $totalRows;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $query = CmsCategory::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

            $rows = $query->get();
        } else {
            $search = $request->input('search.value');
            $rows = CmsCategory::where('title', 'LIKE', "{$search}%")
                            ->orWhere('slug', 'LIKE', "{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            $totalFiltered = $rows->count();
        }

        $data = [];
        $userCanEdit = auth()->user()->can('edit', 'cms_category');
        $userCanDelete = auth()->user()->can('delete', 'cms_category');
        foreach ($rows as $row) {
            $nestedData['id'] = $row->id;
            $nestedData['parent_id'] = $row->parent_id;
            $nestedData['parent_title'] = isset($row->parent) ? $row->parent->title : '-';
            $nestedData['order'] = $row->order;
            $nestedData['title'] = $row->title;
            $nestedData['slug'] = $row->slug;
            $nestedData['typeAlias'] = $this->typeMap[$row->type];
            $nestedData['type'] = $row->type;
            $nestedData['visible'] = $row->visible;
            $nestedData['external_link'] = $row->external_link;

            $editAction = '<button class="btn bg-gradient-secondary btn-sm action update" data-id="'.$row->id.'" data-toggle="modal" data-target="#cms-category-update-modal"><i class="fa fa-edit mr-0" style="width: 11px"></i></button>';
            $deleteAction = '<button class="btn bg-gradient-danger btn-sm action delete" data-id="'.$row->id.'"><i class="fas fa-trash"></i></button>';
            $nestedData['actions'] = $userCanEdit ? $editAction : '';
            $nestedData['actions'] .= $userCanDelete ? $deleteAction : '';
            $data[] = $nestedData;
        }

        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        try {
            $this->authorize('create', 'cms_category');

            $this->validate($request, [
                'title' => 'required|regex:/^[\w\p{Han}]{1,25}$/u',
                'slug' => "regex:/^[a-zA-Z0-9_\-]{1,120}$/",
                'order' => 'numeric',
            ], [
                'title.required' => __('lipton::cms.category.feedback.is_required', ['field' => __('lipton::cms.category.modal.title')]),
                'title.regex'    => __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.title')]),
                'slug.regex'     => __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.slug')]),
                'order.numeric'  => __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.order')]),
            ]);

            $category = CmsCategory::create($request->all());

            return response()->json([
                'code' => 0,
                'message' => __('lipton::cms.category.feedback.create_success'),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'cms_category');

            $this->validate($request, [
                'title' => 'required|regex:/^[\w\p{Han}]{1,25}$/u',
                'slug' => "regex:/^[a-zA-Z0-9_\-]{1,120}$/",
                'order' => 'numeric',
            ], [
                'title.required' => __('lipton::cms.category.feedback.is_required', ['field' => __('lipton::cms.category.modal.title')]),
                'title.regex'    => __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.title')]),
                'slug.regex'     => __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.slug')]),
                'order.numeric'  => __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.order')]),
            ]);

            $category = CmsCategory::findOrFail($id);
            $category->parent_id = $request->input('parent_id');
            $category->title = $request->input('title');
            $category->slug = $request->input('slug');
            $category->order = $request->input('order');
            $category->external_link = $request->input('external_link');
            $category->type = $request->input('type');
            $category->save();

            return response()->json([
                'code' => 0,
                'message' => __("lipton::cms.category.feedback.update_success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function visibleToggle(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'cms_category');

            $category = CmsCategory::findOrFail($id);
            $category->visible = $request->input('visible');
            $category->save();
            return response()->json([
                'code' => 0,
                'message' => __("lipton::cms.category.feedback.update_success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $this->authorize('delete', 'cms_category');

            $category = CmsCategory::findOrFail($id);
            $category->delete();
            return response()->json([
                'code' => 0,
                'message' => __("lipton::cms.category.feedback.destroy_success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }
}