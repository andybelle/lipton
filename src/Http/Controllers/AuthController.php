<?php

namespace KevinKao\Lipton\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Log;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function index ()
    {
        return view('lipton::login');
    }

    public function logout (Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('lipton.login');
    }

    public function redirectTo()
    {
        try {
            $lastPath = trim(request()->cookie('lastpath'));
            if (!empty($lastPath)) {
                return $lastPath;
            }
            return route(config('lipton.default_route'));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return route(config('lipton.default_route'));
        }
    }
}