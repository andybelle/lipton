<?php

namespace KevinKao\Lipton\Http\Controllers;

use Illuminate\Http\Request;
use KevinKao\Lipton\Models\Role;
use KevinKao\Lipton\Models\Permission;
use KevinKao\Lipton\Errors;
use Log;
use DB;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('browse', 'role');
        
        try {
            $roles = Role::exceptSuperAdmin()->with('permissions')->get();
            if ($request->ajax()) {
                return response()->json([
                    'code' => 0,
                    'data' => $roles
                ]);
            }

            $permissions = Permission::get();
            return view('lipton::role.index', compact('permissions'));
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode() > 0 ? $e->getCode() : 99,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function datatable(Request $request)
    {
        $this->authorize('browse', 'role');

        $columns = ['id', 'name', 'display_name'];
        $isSuperAdmin = auth()->user()->isSuperAdmin();
        
        $totalRows = $isSuperAdmin ? Role::count() : Role::whereNotIn('name', [config('lipton.super_admin')])->count();
        $totalFiltered = $totalRows;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if (empty($request->input('search.value'))) {
            $rolesQuery = Role::offset($start)
                            ->with('permissions')
                            ->limit($limit)
                            ->orderBy($order, $dir);

            if (! $isSuperAdmin) {
                $rolesQuery->exceptSuperAdmin();
            }
            $roles = $rolesQuery->get();
        } else {
            $search = $request->input('search.value');

            $roles = Role::where('name', 'LIKE', "{$search}%")
                            ->with('permissions')
                            ->orWhere('display_name', 'LIKE', "{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            if (! $isSuperAdmin) {
                $roles = $roles->whereNotIn('name', [config('lipton.super_admin')]);
            }
            $totalFiltered = $roles->count();
        }

        $data = [];
        $userCanEdit = auth()->user()->can('edit', 'role');
        $userCanDelete = auth()->user()->can('delete', 'role');
        foreach ($roles as $role) {
            $nestedData['id'] = $role->id;
            $nestedData['name'] = $role->name;
            $nestedData['display_name'] = __($role->display_name);
            $nestedData['permissions'] = $role->permissions;

            $editAction = '<button class="btn bg-gradient-secondary btn-sm action update" data-id="'.$role->id.'" data-toggle="modal" data-target="#role-update-modal"><i class="fa fa-edit mr-0" style="width: 11px"></i></button>';
            $deleteAction = '<button class="btn bg-gradient-danger btn-sm action delete" data-id="'.$role->id.'"><i class="fas fa-trash"></i></button>';

            $nestedData['actions'] = $userCanEdit ? $editAction : '';
            $nestedData['actions'] .= $userCanDelete ? $deleteAction : '';
            $data[] = $nestedData;
        }

        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('create', 'role');

        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'required',
        ], [
            'name.required' => __('lipton::message.field_required', ['field' => __('lipton::role.modal.name')]),
            'display_name.required' => __('lipton::message.field_required', ['field' => __('lipton::role.modal.display_name')]),
        ]);

        try {
            DB::beginTransaction();

            $role = new Role();
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->save();
            $role->permissions()->attach($request->input('permissions'));

            DB::commit();
            return response()->json([
                'code' => 0,
                'data' => $role
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $roleId)
    {
        $this->authorize('edit', 'role');

        $this->validate($request, [
            'display_name' => 'required',
        ], [
            'display_name.required' => __('lipton::message.field_required', ['field' => __('lipton::role.modal.display_name')]),
        ]);

        try {
            DB::beginTransaction();

            $role = Role::findOrFail($roleId);
            $role->display_name = $request->input('display_name');
            $role->save();
            $role->permissions()->sync($request->input('permissions'));

            DB::commit();
            return response()->json([
                'code' => 0,
                'data' => $role
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy(Request $request, $roleId)
    {
        $this->authorize('delete', 'role');

        try {
            $role = Role::findOrFail($roleId);
            $role->delete();
            return response()->json(['code' => 0]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);;
        }
    }
}