<?php

namespace KevinKao\Lipton\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('lipton.login')->withCookie('lastpath', $request->path());
        }

        $user = Auth::user();
        if ($user->hasRoles(config('lipton.admin_group', [config('lipton.super_admin')]))) {
            return $next($request);
        } else {
            return redirect()->route('lipton.login');
        }
    }
}