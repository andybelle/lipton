<?php

namespace KevinKao\Lipton\Http\View\Composers;

use Illuminate\View\View;

class SidebarComposer
{
    /**
     * @param  
     * @return void
     */
    public function __construct()
    {
        // 
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $nav = [];
        foreach(config('lipton.nav') as $header => $items) {
            $tmp = [];
            foreach($items as $item) {
                $navObj = resolve($item);
                if ($navObj->isVisible()) {
                    $tmp[] = $navObj;
                }
            }
            if (count($tmp) > 0) {
                $nav[$header] = $tmp;
            }
        }
        $view->with('nav', $nav);
    }
}