<?php

namespace KevinKao\Lipton\Console\Commands;

use Illuminate\Console\Command;
use KevinKao\Lipton\Models\CmsCrawler;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\StreamOutput;

class CrawlerList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all crawlers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crawlers = CmsCrawler::all();
        $output = new StreamOutput(fopen('php://stdout', 'w'));
        $table = new Table($output);
        $table->setHeaders(['id', 'category(id)', 'title']);
        foreach($crawlers as $crawler) {
            $category = empty($crawler->category) ? "" : "{$crawler->category->title}({$crawler->cms_category_id})";
            $table->addRow([$crawler->id, $category, $crawler->title]);
        }
        $table->render();
    }
}
