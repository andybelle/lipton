<?php

namespace KevinKao\Lipton\CMS\Crawler\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use KevinKao\Lipton\Constants;
use KevinKao\Lipton\CMS\UserAgent;
use KevinKao\Lipton\Models\CmsCrawler;
use KevinKao\Lipton\CMS\Crawler\Jobs\NewsContentCrawler;
use QL\QueryList;

use Log;

class NewsListCrawler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $crawler;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CmsCrawler $crawler)
    {
        $this->crawler = $crawler;

        // if (isset($this->crawler->listRule)) {
        //     $this->startUrlSegments = parse_url($this->crawler->listRule->start_url);
        // }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $rule = $this->crawler->listRule;
            Log::channel('crawler')->info("{$this->crawler->title} List page: {$rule->start_url}");
            Log::channel('crawler')->info("start: {$rule->start_number}, end: {$rule->end_number}, current: {$rule->current_number}");
            $urlList = [];
            $dispatchCount = 0;

            if ($rule->start_number > 0 && $rule->end_number > 0) {

                if (!preg_match('/\%number\%/', $rule->start_url)) {
                    Log::channel('crawler')->info("Start url dose not contain %number% keyword, stop crawler.");
                    return;
                }

                if ($rule->current_number < 0) {
                    $this->crawler->listRule()->update([
                        'current_number' => $rule->start_number
                    ]);
                    $rule->current_number = $rule->start_number;
                }

                if ($rule->current_number >= $rule->end_number) {
                    $urlList = [str_replace("%number%", $rule->current_number, $rule->start_url)];
                    $this->crawler->listRule()->update([
                        'current_number' => $rule->current_number - 1
                    ]);
                } else if ($rule->current_number < $rule->end_number) {
                    $keepCrawlingWhenNumberEnd = $rule->followup_action == Constants::CMS_CRAWLER_FOLLOWACTION_1;
                    $stopCrawlingWhenNumberEnd = $rule->followup_action == Constants::CMS_CRAWLER_FOLLOWACTION_2;
                    if ($keepCrawlingWhenNumberEnd) {
                        $urlList = [str_replace("%number%", $rule->end_number, $rule->start_url)];
                    }
                }
            } else {
                if (!preg_match('/\%number\%/', $rule->start_url)) {
                    $urlList = explode(PHP_EOL, $rule->start_url);
                }
            }

            Log::channel('crawler')->debug($urlList);

            foreach($urlList as $startUrl) {
                $startUrlSegments = parse_url($startUrl);
                $ql = QueryList::get($startUrl, [], [
                            'headers' => [
                                'User-Agent' => UserAgent::random()
                            ]
                        ])
                        ->rules([
                            'link' => [$rule->selector, 'href']
                        ])
                        ->range($rule->range)
                        ->query();

                $data = array_slice($ql->getData()->all(), 0, $rule->limit);
                foreach($data as $item) {
                    $url = isset($item['link']) ? $this->fixUrl($item['link'], $startUrlSegments) : '';
                    if ($item && $this->validateUrl($url)) {
                        NewsContentCrawler::dispatch($this->crawler, $url)->onQueue('crawler.news');
                        $dispatchCount++;
                    }
                }
            }
            
            Log::channel('crawler')->info(
                __('lipton::cms.crawler.job.dispatched_crawler_quantity', ['quantity' => $dispatchCount])
            );
            Log::channel('crawler')->info(__('lipton::cms.crawler.job.collect_finish'));
        } catch (\Exception $e) {
            Log::channel('crawler')->error($e->getMessage());
        }
    }

    private function validateUrl($url)
    {
        $pattern = '%\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))%s';
        return preg_match($pattern, $url);
    }

    private function fixUrl($src, $startUrlSegments) {
        try {
            $parts = parse_url($src);
            $url = '';
            $url = isset($parts['scheme']) ? $parts['scheme'] : $startUrlSegments['scheme'];
            $url = isset($parts['host']) ? "{$url}://{$parts['host']}" : "{$url}://{$startUrlSegments['host']}";
            $url = isset($parts['port']) ? "{$url}:{$parts['port']}" : $url;
            $url = "{$url}{$parts['path']}";
            return $url;
        } catch (\Exception $e) {
            Log::channel('crawler')->error($e->getMessage());
            return $src;
        }
    }
}
