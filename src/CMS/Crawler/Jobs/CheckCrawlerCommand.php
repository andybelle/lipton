<?php

namespace KevinKao\Lipton\CMS\Crawler\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use KevinKao\Lipton\Models\CmsCrawler;
use KevinKao\Lipton\CMS\CrawlerCommand;
use KevinKao\Lipton\CMS\Crawler\Jobs\NewsListCrawler;

use Log;

class CheckCrawlerCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $crawler;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $query = CmsCrawler::pending();
            $crawlers = $query->get();
            $query->update(['command_status' => CrawlerCommand::EXECUTING]);
            $matches = $crawlers->pluck('id');

            foreach($crawlers as $crawler) {
                Log::channel('crawler')->info("{$crawler->title} executing.");
                NewsListCrawler::dispatch($crawler)->onQueue('crawler.news');
            }
        } catch (\Exception $e) {
            Log::channel('crawler')->error($e->getMessage());
        } finally {
            CmsCrawler::whereIn('id', $matches)->update(['command_status' => CrawlerCommand::SUSPENDED]);
        }
    }
}
