<?php

namespace KevinKao\Lipton\CMS\Crawler\Middleware\Content;

use Log;
use Storage;
use KevinKao\Lipton\Models\CmsMedia;
use \KevinKao\Lipton\Constants;

class ImageDownload
{
    protected $exMap;
    protected $mediaArr;
    protected $downloaded;

    public function __construct()
    {
        $this->exMap = [
            IMAGETYPE_GIF  => 'gif',
            IMAGETYPE_JPEG => 'jpg',
            IMAGETYPE_PNG  => 'png',
            IMAGETYPE_BMP  => 'bmp'
        ];
        $this->mediaArr = [];
        $this->downloaded = [];
    }

    public function handle($data, $crawler, $url)
    {
        if ($crawler->image_solution == 0) {
            return $data;
        }
        if (!empty($data['cover']) && $media = $this->download($data['cover'])) {
            $data['cover_id'] = $media->id;
            $this->mediaArr[] = $media;
        }
        $data['content'] = $this->scan($data['content']);
        $data['mediaArr'] = $this->mediaArr;
        return $data;
    }

    private function scan($content)
    {
        if (preg_match('/<img[^>]* src=\"(https?:\/\/[^\"]*)\"[^>]*>/', $content, $matches)) {
            $path = '#';
            if ($media = $this->download($matches[1])) {
                $path = $media->path;
            }

            $replaced = preg_replace(
                '/<img[^>]* src=\"(https?:\/\/[^\"]*)\"[^>]*>/',
                '<img src="'.$path.'">',
                $content,
                1
            );

            $this->mediaArr[] = $media;
            return $this->scan($replaced);
        }
        return $content;
    }

    private function download($url)
    {
        try {
            Log::channel('crawler')->debug("Download image: {$url}");

            if (array_key_exists($url, $this->downloaded)) {
                Log::channel('crawler')->debug("{$url} has downloaded.");
                return $this->downloaded[$url];
            }

            $urlParts = parse_url($url);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_AUTOREFERER, false);
            curl_setopt($ch, CURLOPT_REFERER, "{$urlParts['scheme']}://{$urlParts['host']}");
            $result = curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $tmpFile = "/tmp/" . uniqid();
            $fp = fopen($tmpFile, 'w');
            fwrite($fp, $result);
            fclose($fp);
            Log::channel('crawler')->debug("{$code} {$tmpFile}");

            $extension = $this->exMap[exif_imagetype($tmpFile)];
            $path = config('lipton.media_path') . '/' . md5(uniqid().time()) . ".{$extension}";
            $media = null;
            if (Storage::disk('public')->put($path, $result)) {
                $media = new CmsMedia();
                $media->path = Storage::url($path);
                $media->type = Constants::CMS_MEDIA_TYPE_IMAGE;
                $media->save();

                $this->downloaded[$url] = $media;
            }
            unlink($tmpFile);

            return isset($media) ? $media : false;
        } catch (\Exception $e) {
            Log::channel('crawler')->error($e->getMessage());
        }
    }
}