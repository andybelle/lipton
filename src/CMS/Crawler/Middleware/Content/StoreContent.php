<?php

namespace KevinKao\Lipton\CMS\Crawler\Middleware\Content;

use Log;
use DB;
use \KevinKao\Lipton\Models\CmsPost;
use \KevinKao\Lipton\Models\CmsMedia;
use \KevinKao\Lipton\Models\User;
use \KevinKao\Lipton\Constants;

class StoreContent
{
    public function handle($data, $crawler, $url)
    {
        if (!$crawler->category) {
            Log::channel('crawler')->info(__('lipton::cms.crawler.job.not_specify_category'));
            return $data;
        }

        $post = new CmsPost();
        $post->cms_category_id = $crawler->cms_category_id;
        $post->title = $data['title'];
        $post->content = $data['content'];
        $post->author_id = User::first()->id;
        if (!empty($data['cover_id'])) {
            $post->cover_id = $data['cover_id'];
        } else if (!empty($data['cover'])) {
            $media = new CmsMedia();
            $media->path = $data['cover'];
            $media->type = Constants::CMS_MEDIA_TYPE_IMAGE;
            $media->save();
            $post->cover_id = $media->id;
        }
        $post->save();

        if (isset($data['mediaArr'])) {
            foreach($data['mediaArr'] as $media) {
                $media->title = $post->title;
                $media->post_id = $post->id;
                $media->save();
            }
        }

        return $data;
    }
}