<?php

namespace KevinKao\Lipton\CMS\Crawler\Middleware\Content;

use Log;
use \KevinKao\Lipton\Models\CmsPost;

class CheckDuplicate
{
    public function handle($data, $crawler, $url)
    {
        $post = CmsPost::where('title', $data['title'])->first();
        if (empty($post)) {
            return $data;
        }
        // Duplicated
        Log::channel('crawler')->debug("{$data['title']} duplicated");
        return false;
    }
}