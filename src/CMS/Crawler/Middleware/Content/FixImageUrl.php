<?php

namespace KevinKao\Lipton\CMS\Crawler\Middleware\Content;

use Log;

class FixImageUrl
{
    public function handle($data, $crawler, $url)
    {
        $urlSegments = parse_url($url);

        $replaced = preg_replace(
            '/<img[^>]* src=\"(\/\/[^\"]*)\"[^>]*>/',
            "<img src=\"{$urlSegments['scheme']}:$1\">",
            $data['content']
        );
        $replaced = preg_replace(
            '/<img[^>]* src=\"(\/[^\"]*)\"[^>]*>/',
            "<img src=\"{$urlSegments['scheme']}://{$urlSegments['host']}$1\">",
            $replaced
        );

        $data['content'] = $replaced;
        if (!empty($data['cover'])) {
            $parts = parse_url($data['cover']);
            
            $url = '';
            $url = isset($parts['scheme']) ? $parts['scheme'] : $urlSegments['scheme'];
            $url = isset($parts['host']) ? "{$url}://{$parts['host']}" : "{$url}://{$urlSegments['host']}";
            $url = isset($parts['port']) ? "{$url}:{$parts['port']}" : $url;
            $url = "{$url}{$parts['path']}";
            $data['cover'] = $url;
        }

        return $data;
    }
}