<?php

namespace KevinKao\Lipton\CMS;

class CrawlerCommand
{
    const PENDING = 0;
    const EXECUTING = 1;
    const SUSPENDED = 2;
}