@extends('lipton::layout')

@push('pre-styles')
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.css') }}">
@endpush

@push('styles')
<style>
    table .action {
        margin-right: 0;
    }
    table th.text-center,
    table td.text-center {
        text-align: center;
    }
</style>
@endpush

@section('page_toolbar')
<div class="toolbar">
    <button class="btn btn-success" id="fresh-table">{{ __('lipton::cms.comment.toolbar.fresh') }}</button>
    {{-- @can('create', 'cms_comment')
    <button class="btn btn-primary create-cms-comment" data-toggle="modal" data-target="#cms-comment-create-modal">
        {{ __('lipton::cms.comment.toolbar.create') }}
    </button>
    @endcan --}}
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('lipton::cms.nav.comment.index') }}</li>
    </ol>
@endsection

@section('container')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <th>{{ __('lipton::cms.comment.table.post_title') }}</th>
                            <th>{{ __('lipton::cms.comment.table.author') }}</th>
                            <th>{{ __('lipton::cms.comment.table.content_brief') }}</th>
                            <th>{{ __('lipton::cms.comment.table.created_at') }}</th>
                            <th>{{ __('lipton::cms.comment.table.status') }}</th>
                            <th>{{ __('lipton::cms.comment.table.action') }}</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade cms-comment-modal" id="cms-comment-modal" tabindex="-1" role="dialog" aria-labelledby="create-form-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="content"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::role.modal.close') }}</button>
                    {{-- <button type="button" class="btn btn-primary">{{ __('lipton::role.modal.submit') }}</button> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ Lipton::assets('/js/validator.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validation.js') }}"></script>
<script src="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.js') }}"></script>
@datatable_locale()
<script>
    $('#fresh-table').on('click', function() {
        $('#datatable').DataTable().ajax.reload(null, false);
    });
    $('#datatable').DataTable({
        "autoWidth": false,
        "serverSide": true,
        "processing": true,
        "stateSave": true,
        "ajax": {
            url: '{{ route('lipton.cms.comment.datatable') }}',
            type: 'post'
        },
        "createdRow": function(row, data, dataIndex) {
            $(row).data(data);
        },
        "columns": [
            { "data": "post_title_brief", "orderable": false, "searchable": false, "width": "25%" },
            { "data": "author", "orderable": false, "width": "15%" },
            { "data": "contnet_brief", "orderable": false, "width": "20%", render: function(data, type, row) {
                return '<a href="javascript:void(0)" data-toggle="modal" data-target="#cms-comment-modal">'+data+'</a>';
            } },
            { "data": "created_at", "width": "15%" },
            { "data": "status", "width": "10%", "className": "text-center", render: function(data, type, row) {
                var isChecked = (data == 1);
                var $elem = $('<input class="status-toggle" data-idx="'+row.id+'" type="checkbox" name="status" '+ (isChecked ? 'checked="checked"' : '')+'>');
                if (!row.canEdit) {
                    $elem.attr('disabled', true);
                }
                return $elem.prop('outerHTML');;
            } },
            { "data": "actions", "searchable": false, "orderable": false, "width": "10%", "className": "text-center", render: function(data, type, row) {
                var $elem = $('<button/>')
                    .addClass('btn bg-gradient-danger btn-sm action delete').data('id', row.id)
                    .html('<i class="fas fa-trash"></i>')
                if (!row.canDelete) {
                    $elem.prop('disabled', true).addClass('disabled');
                }
                return $elem.prop('outerHTML');
            } }
        ],
        "order": [[ 4, "asc" ]],
        "drawCallback": function() {
            $('.status-toggle', this).bootstrapToggle({ size: 'sm' });
        }
    }).on('click', '.delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::common.actions.delete_confirmed') }}',
            text: "{{ __('lipton::common.actions.delete_hint') }}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::common.actions.delete_confirm_text') }}',
            cancelButtonText: '{{ __('lipton::common.actions.delete_cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ route('lipton.cms.comment.destroy', ':id') }}'.replace(':id', id),
                    type: 'delete'
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                });
            }
        });
    }).on('change', '.status-toggle', function() {
        var self = this;
        var id = $(this).data('idx');

        $(self).prop('disabled', true);
        $.ajax({
            url: '{{ route('lipton.cms.comment.status.toggle', ':id') }}'.replace(':id', id),
            type: 'put',
            dataType: 'json',
            data: JSON.stringify({ status: $(this).prop('checked') }),
            headers: { 'Content-type': 'application/json' }
        }).done(function(resp) {
            if (resp.code > 0) {
                Toast.fire({ type: 'error', title: resp.message });
                return false;
            }
            Toast.fire({ type: 'success', title: resp.message });
        }).always(function() {
            $(self).prop('disabled', false);
        });
    });

    $('#cms-comment-modal')
        .on('show.bs.modal', function(e) {
            var $button = $(e.relatedTarget);
            var data = $button.parents('tr').data();
            var $modal = $('#cms-comment-modal');

            console.log(data);
            $('.modal-title', this).html(data.post_title);
            $(".content", this).html(data.content);
        });
</script>
@endpush