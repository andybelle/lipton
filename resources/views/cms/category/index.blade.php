@extends('lipton::layout')

@push('pre-styles')
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.css') }}">
@endpush

@section('page_toolbar')
<div class="toolbar">
    <button class="btn btn-success" id="fresh-table">{{ __('lipton::cms.category.toolbar.fresh') }}</button>
    @can('create', 'cms_category')
    <button class="btn btn-primary create-cms-category" data-toggle="modal" data-target="#cms-category-create-modal">
        {{ __('lipton::cms.category.toolbar.create') }}
    </button>
    @endcan
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('lipton::cms.nav.category.index') }}</li>
    </ol>
@endsection

@section('container')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <th>{{ __('lipton::cms.category.table.order') }}</th>
                            <th>{{ __('lipton::cms.category.table.title') }}</th>
                            <th>{{ __('lipton::cms.category.table.parent') }}</th>
                            <th>{{ __('lipton::cms.category.table.slug') }}</th>
                            <th>{{ __('lipton::cms.category.table.type') }}</th>
                            <th>{{ __('lipton::cms.category.table.visible') }}</th>
                            <th>{{ __('lipton::cms.category.table.action') }}</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade cms-category-modal" id="cms-category-create-modal" tabindex="-1" role="dialog" aria-labelledby="create-form-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="create-form-label">{{ __('lipton::cms.category.modal.title_create') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.title') }}<span style="color: red">*</span></label>
                            <input name="title" type="text" class="form-control" placeholder="example">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.parent_id') }}</label>
                            <select name="parent_id" class="form-control">
                                <option value="">{{ __('lipton::cms.category.modal.no_parent') }}</option>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.slug') }}(slug)</label>
                            <input name="slug" type="text" class="form-control" placeholder="example-title-slug">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.order') }}</label>
                            <input name="order" type="number" class="form-control" placeholder="5" min="1">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.external_link') }}</label>
                            <input name="external_link" type="text" class="form-control" placeholder="https://example.com">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="inlineRadio1" value="1" checked="checked">
                            <label class="form-check-label" for="inlineRadio1">
                                {{ __('lipton::cms.category.modal.type_general') }}
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="inlineRadio2" value="2">
                            <label class="form-check-label" for="inlineRadio2">
                                {{ __('lipton::cms.category.modal.type_external_link') }}
                            </label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::role.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::role.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade cms-category-modal" id="cms-category-update-modal" tabindex="-1" role="dialog" aria-labelledby="update-form-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="update-form-label">{{ __('lipton::cms.category.modal.title_update') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.title') }}<span style="color: red">*</span></label>
                            <input name="title" type="text" class="form-control" placeholder="example">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.parent_id') }}</label>
                            <select name="parent_id" class="form-control">
                                <option value="">{{ __('lipton::cms.category.modal.no_parent') }}</option>
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.slug') }}(slug)</label>
                            <input name="slug" type="text" class="form-control" placeholder="example-title-slug">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.order') }}</label>
                            <input name="order" type="number" class="form-control" placeholder="5" min="1">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.category.modal.external_link') }}</label>
                            <input name="external_link" type="text" class="form-control" placeholder="https://example.com">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="updateRadio1" value="1" checked="checked">
                            <label class="form-check-label" for="updateRadio1">
                                {{ __('lipton::cms.category.modal.type_general') }}
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type" id="updateRadio2" value="2">
                            <label class="form-check-label" for="updateRadio2">
                                {{ __('lipton::cms.category.modal.type_external_link') }}
                            </label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::role.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::role.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ Lipton::assets('/js/validator.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validation.js') }}"></script>
<script src="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.js') }}"></script>
@datatable_locale()
<script>
    $('#fresh-table').on('click', function() {
        $('#datatable').DataTable().ajax.reload(null, false);
    });
    $('.cms-category-modal').on('hidden.bs.modal', function() {
        $('input', this).not('[type=radio]').val('');
    });
    $('#datatable').DataTable({
        "autoWidth": false,
        "serverSide": true,
        "processing": true,
        "stateSave": true,
        "ajax": {
            url: '{{ route('lipton.cms.category.datatable') }}',
            type: 'post'
        },
        "createdRow": function(row, data, dataIndex) {
            $(row).data(data);
        },
        "columns": [
            { "data": "order", "searchable": false, "width": "10%" },
            { "data": "title", "orderable": false, "width": "15%" },
            { "data": "parent_title", "orderable": false, "width": "15%" },
            { "data": "slug", "orderable": false, "width": "10%" },
            { "data": "typeAlias", "searchable": false, "orderable": false, "width": "10%" },
            { "data": "visible", "searchable": false, "orderable": false, "width": "10%", render: function(data, type, row) {
                var isVisible = (data == 1);
                return '<input class="visible-toggle" data-idx="'+row.id+'" type="checkbox" name="visible" '+ (isVisible ? 'checked="checked"' : '')+'>';
            } },
            { "data": "actions", "searchable": false, "orderable": false, "width": "10%" }
        ],
        "drawCallback": function() {
            $('.visible-toggle', this).bootstrapToggle({ size: 'sm' });
        }
    }).on('click', '.delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::cms.category.message.delete_confirmed') }}',
            text: "{{ __('lipton::cms.category.message.delete_tip') }}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::cms.category.message.confirm_text') }}',
            cancelButtonText: '{{ __('lipton::cms.category.message.cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ route('lipton.cms.category.destroy', ':id') }}'.replace(':id', id),
                    type: 'delete'
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                });
            }
        });
    }).on('change', '.visible-toggle', function() {
        var self = this;
        var id = $(this).data('idx');

        $(self).prop('disabled', true);
        $.ajax({
            url: '{{ route('lipton.cms.category.visible.toggle', ':id') }}'.replace(':id', id),
            type: 'put',
            dataType: 'json',
            data: JSON.stringify({ visible: $(this).prop('checked') }),
            headers: { 'Content-type': 'application/json' }
        }).done(function(resp) {
            if (resp.code > 0) {
                Toast.fire({ type: 'error', title: resp.message });
                return false;
            }
            Toast.fire({ type: 'success', title: resp.message });
        }).always(function() {
            $(self).prop('disabled', false);
        });
    });

    $('#cms-category-create-modal')
        .on('show.bs.modal', function(e) {
            $('.overlay', '#cms-category-create-modal').removeClass('hidden').addClass('shown');
            $.get('{{ route('lipton.cms.category.index') }}')
                .done(function(resp) {
                    var $select = $('select[name=parent_id]', '#cms-category-create-modal');
                    $select.find('option').slice(1).remove();
                    $.each(resp.data, function(_, item) {
                        $('<option>', { value: item.id }).html(item.title).appendTo($select);
                    });
                })
                .always(function() {
                    $('.overlay', '#cms-category-create-modal').removeClass('shown').addClass('hidden');
                });
        })
        .on('click', '.btn-primary', function() {
            var $form = $(this).parents('.modal-content').find('form');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                title: {
                    required: {
                        message: "{{ __('lipton::cms.category.feedback.is_required', ['field' => __('lipton::cms.category.modal.title')]) }}"
                    },
                    regexp: {
                        args: [/^[\w\u4e00-\u9fa5]{1,25}$/],
                        message: "{{ __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.title')]) }}"
                    }
                },
                slug: {
                    regexp: {
                        args: [/^[a-zA-Z0-9_\-]{1,120}$/],
                        message: "{{ __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.slug')]) }}"
                    }
                },
                order: {
                    regexp: {
                        args: [/^\d+$/],
                        message: "{{ __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.order')]) }}"
                    }
                }
            }, function() {
                var form = this;
                var data = {};
                $('input.form-control', form).each(function(_, ele) {
                    if (/.+/.test(this.value)) {
                        data[this.name] = this.value;
                    }
                });
                data['parent_id'] = $('select[name=parent_id]', form).val();
                data['type'] = $('input[type=radio]:checked', form).val();
                $('.overlay', '#cms-category-create-modal').removeClass('hidden').addClass('shown');
                $.ajax({
                    url: '{{ route('lipton.cms.category.store') }}',
                    type: 'post',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return false;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                    $('#cms-category-create-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#cms-category-create-modal').removeClass('shown').addClass('hidden');
                });
            }, function(ele, ruleName, message) {
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        });

    $("#cms-category-update-modal")
        .on('click', '.btn-primary', function() {
            var $form = $(this).parents('.modal-content').find('form');
            var categoryId = $(this).data('id');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                title: {
                    required: {
                        message: "{{ __('lipton::cms.category.feedback.is_required', ['field' => __('lipton::cms.category.modal.title')]) }}"
                    },
                    regexp: {
                        args: [/^[\w\u4e00-\u9fa5]{1,25}$/],
                        message: "{{ __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.title')]) }}"
                    }
                },
                slug: {
                    regexp: {
                        args: [/^[a-zA-Z0-9_\-]{1,120}$/],
                        message: "{{ __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.slug')]) }}"
                    }
                },
                order: {
                    regexp: {
                        args: [/^\d+$/],
                        message: "{{ __('lipton::cms.category.feedback.not_match', ['field' => __('lipton::cms.category.modal.order')]) }}"
                    }
                }
            }, function() {
                var form = this;
                var data = {};
                $('input.form-control', form).each(function(_, ele) {
                    if (/.+/.test(this.value)) {
                        data[this.name] = this.value;
                    }
                });
                data['parent_id'] = $('select[name=parent_id]', form).val();
                data['type'] = $('input[type=radio]:checked', form).val();

                $('.overlay', '#cms-category-update-modal').removeClass('hidden').addClass('shown');
                $.ajax({
                    url: '{{ route('lipton.cms.category.update', ':id') }}'.replace(':id', categoryId),
                    type: 'put',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return false;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                    $('#cms-category-update-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#cms-category-update-modal').removeClass('shown').addClass('hidden');
                });
            }, function(ele, ruleName, message) {
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        })
        .on('show.bs.modal', function(e) {
            var $button = $(e.relatedTarget);
            var data = $button.parents('tr').data();
            var $modal = $('#cms-category-update-modal');

            $('.overlay', '#cms-category-update-modal').removeClass('hidden').addClass('shown');
            $.get('{{ route('lipton.cms.category.index.exclude', ':id') }}'.replace(':id', $button.data('id')))
                .done(function(resp) {
                    var $select = $('select[name=parent_id]', '#cms-category-update-modal');
                    $select.find('option').slice(1).remove();
                    $.each(resp.data, function(_, item) {
                        $('<option>', { value: item.id }).html(item.title).appendTo($select);
                    });


                    $('input[name=title]', $modal).val(data.title);
                    $('select[name=parent_id]', $modal).val(data.parent_id);
                    $('input[name=slug]', $modal).val(data.slug);
                    $('input[name=order]', $modal).val(data.order);
                    $('input[name=external_link]', $modal).val(data.external_link);
                    $('input[name=type][value='+data.type+']', $modal).prop('checked', true);
                    $('#cms-category-update-modal .btn-primary').data('id', $button.data('id'));
                })
                .always(function() {
                    $('.overlay', '#cms-category-update-modal').removeClass('shown').addClass('hidden');
                });
        });
</script>
@endpush