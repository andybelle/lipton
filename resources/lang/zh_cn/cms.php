<?php

return [
    'nav' => [
        'category' => [
            'index' => '栏目管理'
        ],
        'post' => [
            'index' => '文章管理',
            'create' => '新增文章',
            'edit' => '编辑文章',
        ],
        'comment' => [
            'index' => '评论管理'
        ],
        'crawler' => [
            'index' => '爬虫管理'
        ]
    ],

    'category' => [
        'type' => [
            'general' => '一般',
            'external_link' => '外部链结',
        ],
        'visible' => [
            'yes' => '显示',
            'no' =>  '隐藏',
        ],
        'toolbar' => [
            'fresh'  => '刷新',
            'create' => '新增'
        ],
        'table' => [
            'id'      => '编号',
            'order'   => '排序',
            'title'   => '名称',
            'parent'  => '父栏目',
            'slug'    => '别名',
            'type'    => '型态',
            'visible' => '显示',
            'action'  => '操作',
        ],
        'modal' => [
            'title_create'       => '新增栏目',
            'title_update'       => '更新栏目',
            'type_general'       => '一般',
            'type_external_link' => '外部链结',
            'title'              => '名称',
            'parent_id'          => '父栏目',
            'slug'               => '别名',
            'order'              => '排序',
            'external_link'      => '外部链结',
            'no_parent'          => '不选择',
        ],
        'feedback' => [
            'create_success'  => '新增成功',
            'update_success'  => '更新成功',
            'destroy_success' => '删除成功',
            'is_required'     => ':field 为必填栏位',
            'not_match'       => ':field 请填写正确内容',
            'bad_chart'       => ':field 含有非法字元',
        ],
        'message' => [
            'delete_confirmed' => '确认删除该栏目吗?',
            'delete_tip'       => '删除后就不能复原',
            'confirm_text'     => '是的',
            'cancel_text'      => '取消'
        ]
    ],

    'post' => [
        'toolbar' => [
            'fresh'  => '刷新',
            'create' => '新增',
            'back'   => '返回',
            'submit' => '储存',
            'submit_and_keep_create' => '储存后继续新增',
            'category_all' => '所有文章',
            'search_keyword' => '标题关键字',
            'search_btn' => '搜寻'
        ],
        'table' => [
            'id'             => '序号',
            'order'          => '排序',
            'title'          => '标题',
            'created_at'     => '时间',
            'slug'           => '别名',
            'category_title' => '栏目',
            'status'         => '状态',
            'action'         => '操作',
        ],
        'form' => [
            'title'         => '标题',
            'category'      => '栏目',
            'no_category'   => '不选择',
            'slug'          => '别名',
            'order'         => '排序',
            'external_link' => '外部链结',
            'score'         => '积分',
            'cover'         => '封面',
            'content'       => '内文',
            'meta_title'    => '附加信息',
            'meta_key'      => '键',
            'meta_value'    => '值',
        ],
        'feedback' => [
            'create_success'  => '新增成功',
            'update_success'  => '更新成功',
            'delete_success'  => '删除成功',
            'is_required'     => ':field 为必填栏位',
            'must_be_numeric' => ':field 必须为数字',
            'invalid_slug'    => '别名必须为英数字组成',
            'invalid_url'     => '连结 必须是完整规范连结 [http(s)://example.com]',
            'invalid_image'   => '封面 格式错误, 请上传正确图片格式'
        ],
        'message' => [
            'delete_confirmed' => '确认删除该文章吗?',
            'delete_tip'       => '删除后就不能复原',
            'confirm_text'     => '是的',
            'cancel_text'      => '取消'
        ]
    ],

    'comment' => [
        'toolbar' => [
            'fresh'  => '刷新',
        ],
        'table' => [
            'post_title'    => '文章',
            'author'        => '作者',
            'title'         => '标题',
            'content_brief' => '内容',
            'status'        => '状态',
            'created_at'    => '时间',
            'action'        => '操作'
        ]
    ],

    'crawler' => [
        'toolbar' => [
            'create' => '新增爬虫',
            'log' => '查看日志'
        ],
        'feedback' => [
            'is_required'              => ':field 为必填栏位',
            'not_match'                => ':field 请填写正确内容',
            'create_success'           => '新增爬虫成功',
            'update_success'           => '更新爬虫成功',
            'destroy_success'          => '删除成功',
            'update_operation_success' => ':title 状态: :status',
            'update_schedule_success'  => '更新排程成功',
            'update_rules_success'     => '更新规则成功',
            'operation_start'          => '启动',
            'operation_stop'           => '停止'
        ],
        'table' => [
            'title'     => '名称',
            'domain'    => '域名',
            'category'  => '栏目',
            'schedule'  => '排程',
            'collect'   => '采集',
            'trigger'   => '觸發',
            'rules'     => '规则',
            'operation' => '运行',
            'action'    => '操作',
        ],
        'modal' => [
            'tab_general' => '基本设置',
            'title'           => '爬虫名称',
            'cms_category_id' => '储存类别',
            'create_title'    => '新增爬虫',
            'update_title'    => '编辑爬虫',
            'schedule_title' => '编辑排程',
            'close' => '关闭',
            'submit' => '提交',
            'log_title' => '运行日志',
            'form' => [
                'title'       => '爬虫名称',
                'title_placeholder' => '虎扑足球',
                'domain' => '域名',
                'domain_placeholder' => 'www.example.com, 不需要http(s)://',
                'category'    => '储存栏目',
                'no_category' => '不选择',
                'image_plan' => '图片处理',
                'image_plan_pass' => '不处理',
                'image_plan_download' => '下载到本地端',
                'charset' => '编码'
            ],
            'schedule' => [
                'method' => '频率',
            ]
        ],
        'rules' => [
            'tab_list' => '起始页',
            'tab_content' => '内容页',
            'list_start_url' => 'URL地址',
            'list_start_url_hint' => '输入起始页面位址',
            'limit' => '每次抓取数量',
            'list_range' => '区块选择器',
            'list_selector' => '连结选择器',
            'list_selector_hint' => '提取内容页面URL',
            'crawling' => '抓取中',
            'just_test' => '测试一下',
            'clean_test_content' => '清空测试数据',
            'add_column' => '新增字段',
            'test_content_url' => '测试网址',
            'test_content_url_hint' => '请填入完整的连结',
            'url_number_hint' => '加入%number%会代入号码',
            'start_number' => '起始号码',
            'end_number' => '结束号码',
            'number_hint_1' => '1. 如不需要依照号码采集的话, 起始号码及结束号码设置为 0 即可',
            'number_hint_2' => '2. 采集顺序为起始号码倒序采集到结束号码',
            "when_number_end" => '当号码结束时',
            'keep_crawl_last_number' => '只采集结束号码',
            'stop_crawling' => '终止采集'
        ],
        'message' => [
            'delete_confirmed' => '确认删除该爬虫吗?',
            'delete_tip'       => '删除后就不能复原',
            'confirm_text'     => '是的',
            'cancel_text'      => '取消',
            'trigger_confirmed' => '确认触发爬虫任务吗?'
        ],
        'job' => [
            'not_specify_category' => '爬虫任务未指定储存栏目,略过不储存',
            'dispatched_crawler_quantity' => '派发爬虫任务数量 :quantity',
            'collect_finish' => '采集完毕',
        ],
        'frequency' => [
            0  => '每分钟一次',
            1  => '五分钟一次',
            2  => '十分钟一次',
            3  => '十五分钟一次',
            4  => '三十分钟一次',
            5  => '每小时一次',
            6  => '两小时一次',
            7  => '三小时一次',
            8  => '四小时一次',
            9  => '每天一次',
            10 => '每周一次',
            11 => '每月一次'
        ]
    ]
];