<?php

return [
    'button' => [
        'submit' => '更新',
        'upload' => '上传'
    ],
    'nav' => [
        'index' => '参数设置'
    ],
    'toggle' => [
        'on' => '开启',
        'off' => '关闭'
    ],
    'description' => [
        'site_title' => '站名',
        'site_description' => '网站描述',
        'site_keyword' => '网站关键字',
        'site_analysis_code' => '网站统计代码'
    ],
    'message' => [
        'update_success' => '更新成功'
    ]
];