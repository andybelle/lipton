<?php

return [
    'nav' => [
        'general' => '用戶管理',
        'system' => '系統',
        'cms' => '內容管理',
        'home' => '首頁',
        'logout' => '登出',
        'profile' => '個人資料',
    ],
    'profile' => [
        'title' => '個人資料'
    ],
    'actions' => [
        'success'             => '操作成功',
        'fail'                => '操作失敗 (:message)',
        'delete_confirmed'    => '確認刪除嗎?',
        'delete_hint'         => '刪除後無法復原！',
        'delete_confirm_text' => '是的',
        'delete_cancel_text'  => '取消',
    ]
];