<?php

return [
    'nav' => [
        'category' => [
            'index' => '欄目管理'
        ],
        'post' => [
            'index' => '文章管理',
            'create' => '新增文章',
            'edit' => '編輯文章',
        ],
        'comment' => [
            'index' => '留言管理'
        ],
        'crawler' => [
            'index' => '爬蟲管理'
        ],
    ],

    'category' => [
        'type' => [
            'general' => '一般',
            'external_link' => '外部鏈結',
        ],
        'visible' => [
            'yes' => '顯示',
            'no' =>  '隱藏',
        ],
        'toolbar' => [
            'fresh'  => '刷新',
            'create' => '新增'
        ],
        'table' => [
            'id'      => '編號',
            'order'   => '排序',
            'parent'  => '父欄目',
            'title'   => '名稱',
            'slug'    => '別名',
            'type'    => '型態',
            'visible' => '顯示',
            'action'  => '操作',
        ],
        'modal' => [
            'title_create'       => '新增欄目',
            'title_update'       => '更新欄目',
            'type_general'       => '一般',
            'type_external_link' => '外部鏈結',
            'title'              => '名稱',
            'parent_id'          => '父欄目',
            'slug'               => '別名',
            'order'              => '排序',
            'external_link'      => '外部鏈結',
            'no_parent'          => '不選擇',
        ],
        'feedback' => [
            'create_success'  => '新增成功',
            'update_success'  => '更新成功',
            'destroy_success' => '刪除成功',
            'is_required'     => ':field 為必填欄位',
            'not_match'       => ':field 請填寫正確內容',
            'bad_chart'       => ':field 含有非法字元',
        ],
        'message' => [
            'delete_confirmed' => '確認刪除該欄目嗎?',
            'delete_tip'       => '刪除後就不能復原',
            'confirm_text'     => '是的',
            'cancel_text'      => '取消'
        ]
    ],

    'post' => [
        'toolbar' => [
            'fresh'  => '刷新',
            'create' => '新增',
            'back'   => '返回',
            'submit' => '儲存',
            'submit_and_keep_create' => '儲存後繼續新增',
            'category_all' => '所有文章',
            'search_keyword' => '標題關鍵字',
            'search_btn' => '搜尋'
        ],
        'table' => [
            'id'             => '序號',
            'order'          => '排序',
            'title'          => '標題',
            'slug'           => '別名',
            'created_at'     => '時間',
            'category_title' => '欄目',
            'status'         => '狀態',
            'action'         => '操作',
        ],
        'form' => [
            'title'         => '標題',
            'category'      => '欄目',
            'no_category'   => '不選擇',
            'slug'          => '別名',
            'order'         => '排序',
            'external_link' => '外部鏈結',
            'score'         => '積分',
            'cover'         => '封面',
            'content'       => '內文',
            'meta_title'    => '附加屬性',
            'meta_key'      => '鍵',
            'meta_value'    => '值',
        ],
        'feedback' => [
            'create_success'  => '新增成功',
            'update_success'  => '更新成功',
            'delete_success'  => '刪除成功',
            'is_required'     => ':field 為必填欄位',
            'must_be_numeric' => ':field 必須為數字',
            'invalid_slug'    => '別名必須為英數字組成',
            'invalid_url'     => '連結 必須是完整規範連結 [http(s)://example.com]',
            'invalid_image'   => '封面 格式錯誤, 請上傳正確圖片格式'
        ],
        'message' => [
            'delete_confirmed' => '確認刪除該文章嗎?',
            'delete_tip'       => '刪除後就不能復原',
            'confirm_text'     => '是的',
            'cancel_text'      => '取消'
        ]
    ],

    'comment' => [
        'toolbar' => [
            'fresh'  => '刷新',
        ],
        'table' => [
            'post_title'    => '文章',
            'author'        => '作者',
            'title'         => '標題',
            'content_brief' => '內容',
            'status'        => '狀態',
            'created_at'    => '時間',
            'action'        => '操作'
        ]
    ],

    'crawler' => [
        'toolbar' => [
            'create' => '新增爬蟲',
            'log' => '查看日誌'
        ],
        'feedback' => [
            'is_required'              => ':field 為必填欄位',
            'not_match'                => ':field 請填寫正確內容',
            'create_success'           => '新增爬蟲成功',
            'update_success'           => '更新爬蟲成功',
            'destroy_success'          => '刪除成功',
            'update_operation_success' => ':title 狀態: :status',
            'update_schedule_success'  => '更新排程成功',
            'update_rules_success'     => '更新規則成功',
            'operation_start'          => '啟動',
            'operation_stop'           => '停止'
        ],
        'table' => [
            'title'     => '名稱',
            'domain'    => '網域',
            'category'  => '欄目',
            'schedule'  => '排程',
            'collect'   => '採集',
            'trigger'   => '觸發',
            'rules'     => '規則',
            'operation' => '運行',
            'action'    => '操作',
        ],
        'modal' => [
            'tab_general' => '基本設置',
            'title'           => '爬蟲名稱',
            'cms_category_id' => '儲存類別',
            'create_title'    => '新增爬蟲',
            'update_title'    => '編輯爬蟲',
            'schedule_title' => '編輯排程',
            'close' => '關閉',
            'submit' => '提交',
            'log_title' => '運行日誌',
            'form' => [
                'title'       => '爬蟲名稱',
                'title_placeholder' => '虎撲足球',
                'domain' => '網域',
                'domain_placeholder' => 'www.example.com, 不需要http(s)://',
                'category'    => '儲存欄目',
                'no_category' => '不選擇',
                'image_plan' => '圖片處理',
                'image_plan_pass' => '不處理',
                'image_plan_download' => '下載到本地端',
                'charset' => '編碼'
            ],
            'schedule' => [
                'method' => '頻率',
            ]
        ],
        'rules' => [
            'tab_list' => '起始頁',
            'tab_content' => '內容頁',
            'list_start_url' => 'URL地址',
            'list_start_url_hint' => '輸入起始頁面位址',
            'limit' => '每次抓取數量',
            'list_range' => '區塊選擇器',
            'list_selector' => '連結選擇器',
            'list_selector_hint' => '提取內容頁面URL',
            'crawling' => '抓取中',
            'just_test' => '測試一下',
            'clean_test_content' => '清空測試數據',
            'add_column' => '新增字段',
            'test_content_url' => '測試網址',
            'test_content_url_hint' => '請填入完整的連結',
            'url_number_hint' => '加入%number%會代入號碼',
            'start_number' => '起始號碼',
            'end_number' => '結束號碼',
            'number_hint_1' => '1. 如不需要依照號碼採集的話, 起始號碼及結束號碼設置為 0 即可',
            'number_hint_2' => '2. 採集順序為起始號碼倒序採集到結束號碼',
            "when_number_end" => '當號碼結束時',
            'keep_crawl_last_number' => '只採集結束號碼',
            'stop_crawling' => '終止採集'
        ],
        'message' => [
            'delete_confirmed' => '確認刪除該爬蟲嗎?',
            'delete_tip'       => '刪除後就不能復原',
            'confirm_text'     => '是的',
            'cancel_text'      => '取消',
            'trigger_confirmed' => '確認觸發爬蟲任務嗎?'
        ],
        'job' => [
            'not_specify_category' => '爬蟲任務未指定儲存欄目,略過不儲存',
            'dispatched_crawler_quantity' => '派發爬蟲任務數量 :quantity',
            'collect_finish' => '採集完畢',
        ],
        'frequency' => [
            0  => '每分鐘一次',
            1  => '五分鐘一次',
            2  => '十分鐘一次',
            3  => '十五分鐘一次',
            4  => '三十分鐘一次',
            5  => '每小時一次',
            6  => '兩小時一次',
            7  => '三小時一次',
            8  => '四小時一次',
            9  => '每天一次',
            10 => '每週一次',
            11 => '每月一次'
        ]
    ]
];