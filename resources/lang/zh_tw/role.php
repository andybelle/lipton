<?php

return [
    'display' => [
        'super_admin' => '超級管理員',
        'admin' => '管理員',
        'general' => '一般用戶',
    ],
    'nav' => [
        'index' => '角色管理'
    ],
    'toolbar' => [
        'create' => '新增',
        'fresh' => '刷新'
    ],
    'table' => [
        'id' => '編號',
        'name' => '鍵值',
        'display_name' => '名稱',
        'action' => '操作'
    ],
    'modal' => [
        'name' => '鍵值',
        'display_name' => '名稱',
        'close' => '關閉',
        'submit' => '提交',
    ],
    'feedback' => [
        'invalid_name' => '請填入正確鍵值',
        'invalid_display_name' => '請填入正確名稱',
        'name_bad_chart' => '名稱僅允許(A-Za-z0-9_)字元, 且不允許空白',
    ],
    'message' => [
        'field_required' => ':field 為必填欄位',
        'create_success' => '新增成功',
        'update_success' => '更新成功',
        'delete_success' => '刪除成功',
        'delete_confirmed' => '確認刪除該角色嗎?',
        'delete_tip' => '刪除後就不能復原',
        'confirm_text' => '是的',
        'cancel_text' => '取消',
        'delete_failed' => '刪除失敗',
        'delete_success' => '刪除成功',
    ]
];