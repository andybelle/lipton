(function($) {
    var defaults = {
        spinnerText: 'Loading...',
        spinnerType: 'grow' // border|grow
    };

    var span = {
        'border': '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span style="margin-left: .5rem">:text</span>',
        'border-only': '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
        'grow': '<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span><span style="margin-left: .5rem">:text</span>',
        'grow-only': '<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>'
    };

    $.fn.spinner = function(action, options) {
        return $(this).each(function() {
            var data = $.extend(defaults, $(this).data(), options);
            var type = /\S+/.test(data.spinnerType) ? data.spinnerType : 'border';

            if (action === 'block') {
                $(this)
                    .data('originSpinnerText', $(this).html())
                    .attr('disabled', true)
                    .html(span[type].replace(':text', data.spinnerText));
            } else if (action === 'unblock') {
                $(this)
                    .attr('disabled', false)
                    .html($(this).data('originSpinnerText'));
            }
        });
    };

})(jQuery)